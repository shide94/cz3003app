package com.example.android.cz3003appv1;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.Random;

import Games.ConnectTheDots;
import Games.Dots;

import static android.R.interpolator.linear;

public class ConnectTheDotsActivity extends AppCompatActivity {

    private ConnectTheDots newGame;
    private int numButtons = 8;
    LinearLayout lin;

    private final String TAG = "ConnectTheDotsActivityClass";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_the_dots);
        newGame = new ConnectTheDots(numButtons);
        lin = new LinearLayout(ConnectTheDotsActivity.this);
        addButtons(numButtons);

    }

    public void addButtons(int numButtons){
        Log.i(TAG, "Enter addButtons!");
        int[][] tempList = positionButtons(numButtons);
        for(int i=0;i<numButtons;i++){
            Log.i(TAG, "left margin: " + tempList[i][0]);
            Log.i(TAG, "top margin: " + tempList[i][1]);

        }
        for (int i = 0; i < numButtons; i++) {
            Log.i(TAG, "loop count: " + i);
            Button myButton = new Button(this);
            myButton.setId(i);
            myButton.setText(Integer.toString(i+1));
            myButton.setTextAppearance(this, R.style.ButtonFontStyle);

            FrameLayout fl = (FrameLayout)findViewById(R.id.activity_connect_the_dots);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(200,200);
            params.leftMargin = tempList[i][0];
            params.topMargin = tempList[i][1];
            fl.addView(myButton, params);
            Dots tempDot = newGame.getAllTheDots().get(i);
            Log.i(TAG, "button added");

        }
    }

    public int[][] positionButtons(int numButtons){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels - 100;
        int width = displayMetrics.widthPixels - 200;
        Log.i(TAG, "height: " + height);
        Log.i(TAG, "width: " + width);
        Random randomGenerator = new Random();
        int[][] positionList = new int[numButtons][2];
        for(int i=0;i<numButtons;i++){
            positionList[i][0] = 0;
            positionList[i][1] = 0;
        }
        int x=0, y=0;
        int j;

        for(int i=0;i<numButtons;i++){
            x = randomGenerator.nextInt(width);
            y = randomGenerator.nextInt(height);
            j=0;
            while(j<=i){
                if(Math.abs(positionList[j][0] - x) <200){
                    x = randomGenerator.nextInt(width);
                    j=0;
                }
                j++;
            }
            positionList[i][0] = x;
            j=0;
            while(j<i){
                if(Math.abs(positionList[j][1] - y) <50){
                    y = randomGenerator.nextInt(height);
                    j=0;
                }
                j++;
            }
            positionList[i][1] = y;
        }
        return positionList;
    }
}
