package com.example.android.cz3003appv1;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button mConnectTheDots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mConnectTheDots = (Button) findViewById(R.id.connectDots);

        mConnectTheDots.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context context = MainActivity.this;
                Class destinationActivity = ConnectTheDotsActivity.class;
                Intent startConnectTheDotsGame = new Intent(context, destinationActivity);
                startActivity(startConnectTheDotsGame);
            }
        });
    }


}
