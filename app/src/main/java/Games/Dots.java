package Games;

/**
 * Created by Shide on 6/3/17.
 */

public class Dots {

    private int sequence_number;
    private int status;

    public Dots(int sequence_number, int status) {
        this.sequence_number = sequence_number;
        this.status = status;
    }

    public int getSequence_number() {
        return sequence_number;
    }

    public void setSequence_number(int sequence_number) {
        this.sequence_number = sequence_number;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
