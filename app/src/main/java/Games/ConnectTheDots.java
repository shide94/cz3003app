package Games;

import java.util.ArrayList;
import java.util.Timer;


public class ConnectTheDots {
    private int numDots;
    private ArrayList<Dots> AllTheDots = new ArrayList<Dots>();
    private Timer timer;


    public ConnectTheDots(int numDots){
        createGame(numDots);
    }

    public int getNumDots() {
        return numDots;
    }

    public void setNumDots(int numDots) {
        this.numDots = numDots;
    }

    public ArrayList<Dots> getAllTheDots() {
        return AllTheDots;
    }

    public void setAllTheDots(ArrayList<Dots> allTheDots) {
        AllTheDots = allTheDots;
    }

    public void createGame(int numDots){
        this.numDots = numDots;
        for(int i =0;i<numDots;i++){
            Dots temp = new Dots(i,0);
            AllTheDots.add(temp);
        }
    }
}
